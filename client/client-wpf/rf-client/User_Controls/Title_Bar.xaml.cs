﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace rf_client.User_Controls
{
	/// <summary>
	/// Interaction logic for Title_Bar.xaml
	/// </summary>
	public partial class Title_Bar : UserControl
	{
		// variables
		Window parent;


		// functions
		public Title_Bar()
		{
			InitializeComponent();
		}


		private void UserControl_Loaded(
			object sender,
			RoutedEventArgs e )
		{
			parent = Window.GetWindow( this );
		}


		private void UserControl_MouseDown(
			object sender,
			MouseButtonEventArgs e )
		{
			if( e.ChangedButton == MouseButton.Left )
				parent.DragMove();
		}
	}
}
