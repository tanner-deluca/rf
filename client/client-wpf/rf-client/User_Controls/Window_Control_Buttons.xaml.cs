﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace rf_client.User_Controls
{
	/// <summary>
	/// Interaction logic for Window_Control_Buttons.xaml
	/// </summary>
	public partial class Window_Control_Buttons : UserControl
	{
		// variables
		Window parent;


		// functions
		public Window_Control_Buttons()
		{
			InitializeComponent();
		}


		private void UserControl_Loaded(
			object sender,
			EventArgs e )
		{
			parent = Window.GetWindow( this );

			if( parent != null )
			{
				parent.SizeChanged += Parent_SizeChanged;
				Parent_SizeChanged( null, null );
			}
		}


		private void Parent_SizeChanged(
			object sender,
			SizeChangedEventArgs e )
		{
			switch( parent.ResizeMode )
			{
				case ResizeMode.CanResize:
				case ResizeMode.CanResizeWithGrip:
					minimize.Visibility = Visibility.Visible;
					maximize.Visibility = Visibility.Visible;
					break;


				case ResizeMode.CanMinimize:
					maximize.Visibility = Visibility.Collapsed;
					break;


				case ResizeMode.NoResize:
					minimize.Visibility = Visibility.Collapsed;
					maximize.Visibility = Visibility.Collapsed;
					break;


				default:
					break;
			}
		}


		private void minimize_Click(
			object sender,
			RoutedEventArgs e )
		{
			parent.WindowState = WindowState.Minimized;
		}


		private void maximize_Click(
			object sender,
			RoutedEventArgs e )
		{
			parent.WindowState = ( parent.WindowState == WindowState.Maximized ) ? WindowState.Normal : WindowState.Maximized;
		}


		private void close_Click(
			object sender,
			RoutedEventArgs e )
		{
			Application.Current.Shutdown();
		}
	}
}
