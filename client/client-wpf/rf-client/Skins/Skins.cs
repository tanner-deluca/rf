﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;


namespace rf_client.Skins
{
	public static class Skins
	{
		// delegates
		public delegate void Changed_Handler();


		// constants


		// variables
		private static string									inuse	= "";

		private static ResourceDictionary						layout	= null;

		private static Dictionary< string, ResourceDictionary >	skins	= new Dictionary< string, ResourceDictionary >();


		public static event Changed_Handler Changed;


		// functions
		public static bool Initialize()
		{
			try
			{
				layout =			new ResourceDictionary() { Source = new Uri( "Skins/Layout.xaml",	UriKind.Relative ) };
				skins.Add( "light",	new ResourceDictionary() { Source = new Uri( "Skins/Light.xaml",	UriKind.Relative ) } );
				skins.Add( "dark",	new ResourceDictionary() { Source = new Uri( "Skins/Dark.xaml",		UriKind.Relative ) } );
				Use( "light" );
				return true;
			}
			catch( Exception e )
			{
				Debug.WriteLine( "failed to load skins\n" + e.ToString() );
				return false;
			}
		}


		public static void Use(
			string skin )
		{
			if( skin == inuse )
				return;

			inuse = skin;


			Application app = Application.Current;
			
			app.Resources.Clear();
			app.Resources.MergedDictionaries.Clear();
			app.Resources.MergedDictionaries.Add( layout		);
			app.Resources.MergedDictionaries.Add( skins[ skin ]	);


			Changed?.Invoke();
		}
	}
}
