﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace rf_client
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		// variables
		private Windows.Login	login	= null;


		// functions
		private void Application_Startup(
			object sender,
			StartupEventArgs e )
		{
			// load the skins
			if( Skins.Skins.Initialize() != true )
				return;


			login = new Windows.Login();
			login.Show();
		}
	}
}
