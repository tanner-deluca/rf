#ifndef WINDOW_MAIN_H
#define WINDOW_MAIN_H


#include <QMainWindow>


namespace Ui
{
	class Window_Main;
}


class Window_Main : public QMainWindow
{
		Q_OBJECT


	public:
		explicit Window_Main( QWidget *parent = 0 );
		~Window_Main();


	private:
		Ui::Window_Main *ui;
};


#endif // WINDOW_MAIN_H
