#include "window_login.h"
#include "ui_window_login.h"

void (*switch_to_main)()	= nullptr;


Window_Login::Window_Login( QWidget *parent ) :
	QMainWindow( parent ),
	ui( new Ui::Window_Login )
{
	ui->setupUi( this );
}


Window_Login::~Window_Login()
{
	delete ui;
}


void Window_Login::Switch_To_Main_Event( void (*event)() )
{
	switch_to_main = event;
}


void Window_Login::on_pushButton_clicked()
{
	if( switch_to_main )
		switch_to_main();
}
