#include <QApplication>

#include "window_login.h"
#include "window_main.h"


Window_Login*	window_login	= nullptr;
Window_Main*	window_main		= nullptr;


void Switch_To_Main();
void Switch_To_Login();


int main(
		int argc,
		char *argv[] )
{
	QApplication a( argc, argv );

	window_login	= new Window_Login();
	window_main		= new Window_Main();

	window_login->Switch_To_Main_Event( Switch_To_Main );
	window_login->show();

	return a.exec();
}


void Switch_To_Main()
{
	window_login->hide();
	window_main->show();
}


void Switch_To_Login()
{
	window_main->hide();
	window_login->show();
}
