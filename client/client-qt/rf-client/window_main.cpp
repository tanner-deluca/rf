#include "window_main.h"
#include "ui_window_main.h"


Window_Main::Window_Main( QWidget *parent ) :
	QMainWindow( parent ),
	ui( new Ui::Window_Main )
{
	ui->setupUi( this );
}


Window_Main::~Window_Main()
{
	delete ui;
}
