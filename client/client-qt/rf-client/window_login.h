#ifndef WINDOW_LOGIN_H
#define WINDOW_LOGIN_H


#include <QMainWindow>


namespace Ui
{
	class Window_Login;
}


class Window_Login : public QMainWindow
{
		Q_OBJECT


	public:
		explicit Window_Login(QWidget *parent = 0);
		~Window_Login();
		void Switch_To_Main_Event( void (*event)() );


	private slots:
		void on_pushButton_clicked();


	private:
		Ui::Window_Login *ui;
};


#endif // WINDOW_LOGIN_H
