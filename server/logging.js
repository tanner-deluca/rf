/**
 * @file 
 *
 * @description
 */


// node requires
const FS	= require( "fs"	);


// 3rd party requires
const Debug				= require( "debug"						);
const Winston			= require( "winston"					);
const Winston_Rotate	= require( "winston-daily-rotate-file"	);


// project requires
const Cmd_Line_Option	= require( "./cmd-line-option.js"	);
const Validator			= require( "./validator.js"			);
const Stds				= require( "./stds.js"				);
const Configuration		= require( "./configuration.js"		);


// type declarations


// constants
const name				= "logging";					/**< the module's name			*/
const debug_module		= Debug( debug_prefix + name );	/**< a debugger for the module	*/

const config_logging_folder							= "logging_folder";	/**< configuration variable for the folder that contains all of the logs						*/
const config_utc									= "utc";			/**< configuration variable for the app using UTC time											*/
const config_error_logger							= "error_logger";	/**< configuration variable for the error logger												*/
const config_logger_name							= "name";			/**< configuration variable for a logger's name													*/
const config_logger_console							= "console";		/**< configuration variable for a logger to print to console									*/
const config_logger_file							= "file";			/**< configuration variable for a logger to print to file										*/
const config_logger_silent							= "silent";			/**< configuration variable for a logger to print or not										*/
const config_logger_rotate							= "rotate";			/**< configuration variable for a logger to rotate												*/
const config_logger_rotate_options					= "rotate_options";	/**< configuration variable for a logger's rotation options (from winston-daily-rotate-file)	*/
const config_logger_rotate_options_frequency		= "frequency";		/**< configuration variable for a logger's frequency rotation option							*/
const config_logger_rotate_options_datePattern		= "datePattern";	/**< configuration variable for a logger's datePattern rotation option							*/
const config_logger_rotate_options_zippedArchive	= "zippedArchive";	/**< configuration variable for a logger's zippedArchive rotation option						*/
const config_logger_rotate_options_maxSize			= "maxSize";		/**< configuration variable for a logger's maxSize rotation option								*/
const config_logger_rotate_options_maxFiles			= "maxFiles";		/**< configuration variable for a logger's maxFiles rotation option								*/




// global variables


// variables
var debug_options =
[
	{ name: debug_module.namespace, summary: "the " + name + " module debug option" },
]; /**< the module's debug options */

var cmd_line_options =
[
	// new Cmd_Line_Option(
	// 	"name",
	// 	"alias",
	// 	type,
	// 	multiple,
	// 	defaultValue,
	// 	"description",
	// 	typeLabel,
	// 	execution_function,
	// 	exit_after ),
]; /**< the module's cmd line options */

var config			= {};		/**< the module's configuration					*/
var logger_configs	= [];		/**< a list of available logger configurations	*/
var loggers			= [];		/**< a list of loggers							*/
var logger_error	= null;		/**< the error logger for the system			*/


// type functions


// event functions
/**
 * logger error handler
 *
 * @param {String} name  the logger's name
 * @param {Object} error the error that occurred
 */
function Logger_Exception(
	name,
	error )
{
	// kill the logger
	Winston.loggers.get( name ).transports.forEach( function( transport ) { transport.silent = true; } );


	// remove the logger from the list
	if( name == logger_error.name )
	{
		Log_Fatal( "error logging failure\n" + error.toString() );
	}
	else
	{
		Log_Error( name + " logging failure\n" + error.toString() );

		for( var i = 0; i < loggers.length; i++ )
		{
			if( name == loggers[ i ].name )
			{
				loggers = loggers.splice( i, 1 );
				break;
			}
		}
	}
}


// functions
/**
 * the module's initialization function
 *
 * @param {Function} callback a callback function() to notify that the module was initialized
 */
function Initialize(
	callback )
{
	Load_Configuration();

	logger_error = Add( config[ config_error_logger ], Winston.format.combine( Winston.format.timestamp({ format: Timestamp_Format }), Winston.format.json() ) );
	logger_error.exceptions.handle( new Winston.transports.File({ filename: config[ config_logging_folder ] + "/crashes.log" }));

	callback();
}


/**
 * loads the module's configuration
 */
function Load_Configuration()
{
	// get the module's configuration
	config = Configuration.Get( name );


	// validate the configuration
	var map_module			= new Map();
	var map_error_logger	= Validation_Map(
		"error",
		true,
		true,
		false,
		true,
		null,
		"YYYY-MM-DD",
		true,
		"10m",
		null
	);

	map_module.set( config_logging_folder, new Validator(
		config_logging_folder,
		"folder",
		"logs/",
		false,
	));

	map_module.set( config_utc, new Validator(
		config_utc,
		"boolean",
		false,
		false,
	));

	map_module.set( config_error_logger, new Validator(
		config_error_logger,
		"object",
		null,
		false,
		map_error_logger,
	));


	var validator = new Validator(
		name,
		"object",
		null,
		false,
		map_module );

	var retval = validator.Validate( config );

	config = retval.corrected;

	if( retval.errors.length > 0 )
		debug_module( "configuration errors\n%O", retval.errors );


	// error logs are always on
	config[ config_error_logger ][ config_logger_file ] = true;


	// check if we should save anything
	Configuration.Update( name, config );
}


/**
 * the module's shutdown function
 *
 * @param {Function} callback a callback function() to notify that the module was shutdown
 */
function Shutdown(
	callback )
{
	var finished = 0;

	if( loggers.length == 0 )
	{
		callback();
	}
	else if( loggers.length == 1 )
	{
		logger_error.on( "close", function()
		{
			debug_module( "%s finished (1/1)", logger_error.name );
			Clear_Empty_Logs();
			callback();
		});

		Winston.loggers.close( logger_error.name  );
	}
	else
	{
		loggers.forEach( function( element )
		{
			element.on( "close", function()
			{
				finished++;
				debug_module( "%s finished (%d/%d)", element.name, finished, loggers.length );

				if( finished == loggers.length )
				{
					Clear_Empty_Logs();
					callback();
				}
				else if( finished == loggers.length - 1 )
				{
					Winston.loggers.close( logger_error.name  );
				}
			});


			if( element.name != logger_error.name  )
				Winston.loggers.close( element.name );
		});
	}
}


/** 
 * returns the log directory
 */
function Directory()
{
	return config[ config_logging_folder ];
}


/**
 * returns a timestamp in the configured time
 */
function Timestamp()
{
	if( config[ config_utc ] == true )
		return new Date().toUTCString();
	else
		return new Date().toLocaleString();
}


/** 
 * returns a timestamp in UTC
 */
function Timestamp_UTC()
{
	return new Date().toUTCString();
}


/** 
 * returns a timestamp in locale
 */
function Timestamp_Locale()
{
	return new Date().toLocaleString();
}


/**
 * returns the app's timestamp format
 */
function Timestamp_Format()
{
	return ( config[ config_utc ] == true ) ? Timestamp_UTC() : Timestamp_Locale(); 
}


/**
 * returns a map of Validator for a logger
 *
 * @param {String}  name          the logger's name
 * @param {Boolean} log_console   true to log to console
 * @param {Boolean} log_file      true to log to file
 * @param {Boolean} silent        true to log
 * @param {Boolean} rotate        true to rotate the log file
 * @param {String}  frequency     rotation option as defined by winston-daily-rotate-file
 * @param {String}  datePattern   rotation option as defined by winston-daily-rotate-file
 * @param {Boolean} zippedArchive rotation option as defined by winston-daily-rotate-file
 * @param {String}  maxSize       rotation option as defined by winston-daily-rotate-file
 * @param {String}  maxFiles      rotation option as defined by winston-daily-rotate-file
 */
function Validation_Map(
	name,
	log_console,
	log_file,
	silent,
	rotate,
	frequency,
	datePattern,
	zippedArchive,
	maxSize,
	maxFiles )
{
	var map_rotate = new Map();

	map_rotate.set( config_logger_rotate_options_frequency, new Validator(
		config_logger_rotate_options_frequency,
		"string",
		frequency,
		true,
		null,
		{ allow_empty: false },
	));

	map_rotate.set( config_logger_rotate_options_datePattern, new Validator(
		config_logger_rotate_options_datePattern,
		"string",
		datePattern,
		true,
		null,
		{ allow_empty: false },
	));

	map_rotate.set( config_logger_rotate_options_zippedArchive, new Validator(
		config_logger_rotate_options_zippedArchive,
		"boolean",
		zippedArchive,
		false,
	));

	map_rotate.set( config_logger_rotate_options_maxSize, new Validator(
		config_logger_rotate_options_maxSize,
		"string",
		maxSize,
		true,
		null,
		{ allow_empty: false },
	));

	map_rotate.set( config_logger_rotate_options_maxFiles, new Validator(
		config_logger_rotate_options_maxFiles,
		"string",
		maxFiles,
		true,
		null,
		{ allow_empty: false },
	));


	var map = new Map();

	map.set( config_logger_name, new Validator(
		config_logger_name,
		"string",
		name,
		false,
		null,
		{ allow_empty: false },
	));

	map.set( config_logger_console, new Validator(
		config_logger_console,
		"boolean",
		log_console,
		false,
	));

	map.set( config_logger_file, new Validator(
		config_logger_file,
		"boolean",
		log_file,
		false,
	));

	map.set( config_logger_silent, new Validator(
		config_logger_silent,
		"boolean",
		silent,
		false,
	));

	map.set( config_logger_rotate, new Validator(
		config_logger_rotate,
		"boolean",
		rotate,
		false,
	));

	map.set( config_logger_rotate_options, new Validator(
		config_logger_rotate_options,
		"object",
		null,
		true,
		map_rotate,
	));


	return map;
}


/**
 * adds a logger to the app
 *
 * @param {Object} logger_config  a logger configuration to create from
 * @param {Object} format         the output format
 */
function Add(
	logger_config,
	format )
{
	try
	{
		debug_module( "creating logger (%s)", logger_config.name );


		// make the log directory
		var logger_name	= logger_config[ config_logger_name ];
		var directory	= config[ config_logging_folder ] + "/" + logger_name;

		FS.mkdirSync( directory, { recursive: true } );


		// create the logger
		Winston.loggers.add( logger_name, {
				format:	format
		});

		var logger = Winston.loggers.get( logger_name );

		logger.name = logger_name;

		logger.on( "error", Logger_Exception.bind( null, logger_name ) );

		if( logger_config[ config_logger_file ] == true )
		{
			if( logger_config[ config_logger_rotate ] == true )
			{
				logger.add( new Winston_Rotate({
					dirname:		directory,
					filename:		logger_name + "_%DATE%.log",
					silent:			logger_config[ config_logger_silent ],
					datePattern:	logger_config[ config_logger_rotate_options ][ config_logger_rotate_options_datePattern		],
					zippedArchive:	logger_config[ config_logger_rotate_options ][ config_logger_rotate_options_zippedArchive	],
					maxSize:		logger_config[ config_logger_rotate_options ][ config_logger_rotate_options_maxSize			],
					maxFiles:		logger_config[ config_logger_rotate_options ][ config_logger_rotate_options_maxFiles		],
					utc:			config[ config_utc ],
				}));
			}
			else
			{
				logger.add( new Winston.transports.File({
					filename:		config[ config_logging_folder ] + logger_name + "/" + logger_name + ".log",
					silent:			logger_config[ config_logger_silent ],
				}));
			}
		}

		if( logger_config[ config_logger_console ] == true )
		{
			Winston.loggers.get( logger_name ).add( new Winston.transports.Console({
			}));
		}

		
		// add to the list	
		loggers.push( logger );


		debug_module( "created logger" );
		return logger;
	}
	catch( error )
	{
		Log_Fatal( `failed to create logger (${logger_config.name})\n${error}` );
	}
}


/**
 * enables or disables a logger
 *
 * @param {String}  name   the logger's name
 * @param {Boolean} silent true to disable, false to enable
 */
function Silent(
	name,
	silent )
{
	if( name != logger_error.name )
		Winston.loggers.get( name ).transports.forEach( function( transport ) { transport.silent = silent; } );
}


/**
 * logs an error
 */
function Log_Error(
	error )
{
	if( logger_error != null )
		logger_error.error( error );
	else
		console.error( new Date().toLocaleString() + "::" + error );
}


/**
 * logs an error and shuts down the system
 */
function Log_Fatal(
	error )
{
	Log_Error( error );
	process.exit();
}


/**
 * clears all empty log files and folders
 */
function Clear_Empty_Logs()
{
	var folder = config[ config_logging_folder ];

	debug_module( "clearing empty files in (%s)", folder );
	require( "child_process" ).execSync( `find ${folder} -type f -empty -print -delete` );
	debug_module( "cleared empty files" );
	debug_module( "clearing empty folders" );
	require( "child_process" ).execSync( `find ${folder} -type d -empty -print -delete` );
	debug_module( "cleared empty folders" );
}


// exports
module.exports =
{
	// App_Module
	name:				name,
	debug_options:		debug_options,
	cmd_line_options:	cmd_line_options,

	Initialize:			Initialize,
	Shutdown:			Shutdown,


	// custom
	Directory:			Directory,
	Timestamp:			Timestamp,
	Timestamp_Format:	Timestamp_Format,
	Validation_Map:		Validation_Map,
	Add:				Add,
	Silent:				Silent,
	Error:				Log_Error,
	Fatal:				Log_Fatal,
};


// executable code


/** eof */