/**
 * @file cmd-line.js
 *
 * @description file for handling command line args
 */


// node requires


// 3rd party requires


// project requires
const App_Data			= require( "./app-data.js"			);
const Cmd_Line_Option	= require( "./cmd-line-option.js"	);


// type declarations


// constants


// global variables


// variables
var options =
[ 
	new Cmd_Line_Option(
		"help",
		"h",
		Boolean,
		false,
		null,
		"prints the application usage guide",
		null,
		Cmd_Line_Help,
		true )
]; /**< a list of command line options to check */


// type functions


// event functions
/**
 * handler for the --help command line arg
 */
function Cmd_Line_Help(
	args )
{
	// compile debug options
	var all_debug_options = [];

	App_Data.app_modules.forEach( function( element ){
		if( element.debug_options != null )
			all_debug_options = all_debug_options.concat( element.debug_options );
	});


	// compile the message
	var help = require( "./help.json" );

	help.forEach( function( element )
	{
		if( typeof element.cmd_line_options != "undefined" )
			element.optionList = options;
		else if( typeof element.debug_options != "undefined" )
			element.content = all_debug_options;
	});

	console.log( require( "command-line-usage" )( help ) );
}


// functions
/**
 * parses command line arguments
 */
function Parse()
{
	debug( "processing the command line" );	


	// parse the args
	const cmd_line_args = require( "command-line-args" )( options, { partial: true } );

	if( typeof cmd_line_args._unknown != "undefined" )
		debug( "unknown cmd line args: %s", cmd_line_args._unknown )


	// execute the options
	options.forEach( function( element ){
		if( typeof cmd_line_args[ element.name ] != "undefined" )
			element.Execute( cmd_line_args[ element.name ] );
	});


	debug( "processed the command line" );	
}


// exports
module.exports =
{
	Parse:	Parse,
};


// executable code
// add the options for each module
App_Data.app_modules.forEach( function( element ){
	if( element.cmd_line_options != null )
		options = options.concat( element.cmd_line_options );
});


/** eof */