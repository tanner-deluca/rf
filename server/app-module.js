/**
 * @file app-module.js
 *
 * @description class definition for app modules
 */


// node requires


// 3rd party requires


// project requires


// type declarations


// constants


// global variables


// variables


// type functions
/**
 * class to handle application modules
 *
 * @param {Object} my_module defined as the export from app-module-template.js (all exported values are optional)
 */
function App_Module(
	my_module )
{
	// variables
	this.name				= ( !my_module.name				) ? "unknown module"	: my_module.name;
	this.debug_options		= ( !my_module.debug_options	) ? []					: my_module.debug_options;
	this.cmd_line_options	= ( !my_module.cmd_line_options	) ? []					: my_module.cmd_line_options;
	this.initialized		= false;
	this.started			= false;
	this.shutdown			= true;


	// functions
	this.Initialize				= App_Module_Initialize;
	this.Initialize_Custom		= my_module.Initialize;
	this.Initialize_Complete	= App_Module_Initialize_Complete;
	this.Start					= App_Module_Start;
	this.Start_Custom			= my_module.Start;
	this.Start_Complete			= App_Module_Start;
	this.Shutdown				= App_Module_Shutdown;
	this.Shutdown_Custom		= my_module.Shutdown;
	this.Shutdown_Complete		= App_Module_Shutdown_Complete;
}


/**
 * the abstracted initialization function for App_Module
 *
 * @param {Function} callback a callback function() to notify that a module was initialized
 */
function App_Module_Initialize(
	callback )
{
	if( typeof this.Initialize_Custom == "undefined" )
	{
		this.initialized	= true;
		this.shutdown		= false;
		callback();
	}
	else
	{
		debug( "initializing module : " + this.name );
		this.Initialize_Custom( App_Module_Initialize_Complete.bind( this, callback ) );
	}
}


/**
 * the abstracted initialization completion function for App_Module
 *
 * @param {Function} callback a callback function() to notify that a module was initialized
 */
function App_Module_Initialize_Complete(
	callback )
{
	this.initialized	= true;
	this.shutdown		= false;

	debug( "initialized module  : " + this.name );
	callback();
}


/**
 * the abstracted start function for App_Module
 *
 * @param {Function} callback a callback function() to notify that a module was started
 */
function App_Module_Start(
	callback )
{
	if( typeof this.Start_Custom == "undefined" )
	{
		this.started	= true;
		this.shutdown	= false;
		callback();
	}
	else
	{
		debug( "starting module : " + this.name );
		this.Start_Custom( App_Module_Start_Complete.bind( this, callback ) );
	}
}


/**
 * the abstracted startup completion function for App_Module
 *
 * @param {Function} callback a callback function() to notify that a module was started
 */
function App_Module_Start_Complete(
	callback )
{
	this.started	= true;
	this.shutdown	= false;

	debug( "started module  : " + this.name );
	callback();
}


/**
 * the abstracted shutdown function for App_Module
 *
 * @param {Function} callback a callback function() to notify that a module was shut down
 */
function App_Module_Shutdown(
	callback )
{
	if( typeof this.Shutdown_Custom == "undefined" )
	{
		this.initialized	= false;
		this.started		= false;
		this.shutdown		= true;
		callback();
	}
	else
	{
		debug( "shutting down module : " + this.name );
		this.Shutdown_Custom( App_Module_Shutdown_Complete.bind( this, callback ) );
	}
}


/**
 * the abstracted shutdown completion function for App_Module
 *
 * @param {Function} callback a callback function() to notify that a module was shutdown
 */
function App_Module_Shutdown_Complete(
	callback )
{
	this.initialized	= false;
	this.started		= false;
	this.shutdown		= true;

	debug( "shutdown module      : " + this.name );
	callback();
}


// event functions


// functions


// exports
module.exports = App_Module;


// executable code


/** eof */