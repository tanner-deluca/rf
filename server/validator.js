/**
 * @file 
 *
 * @description
 */


// node requires
const Path	= require( "path"	);


// 3rd party requires


// project requires


// type declarations


// constants


// global variables


// variables


// type functions
/**
 * class for data validation
 *
 * @param {String} name               the variable's field name
 * @param {String} type               the variable's type
 * @param {*}      default_value      the default value of the variable we're validating (don't need for objects)
 * @param {*}      allow_null         true to allow null values
 * @param {Object} object_validations a map of validations for an object (required only for object types)
 * @param {Object} constraints        constaints to validate against (optional) (different for each data type)
 *
 * types = number, string, boolean, folder, object 
 * folder: all folder types will end in a / or \\ (depending on OS)
 *
 * constraints
 * number: {
 *  minimum: number (optional),
 *  maximum: number (optional),
 * }
 * string: {
 *  allow_empty: boolean (optional),
 *  length: number (optional),
 *  regex: string (optional),
 * }
 */
function Validator(
	name,
	type,
	default_value,
	allow_null,
	object_validations,
	constraints )
{
	this.name				= name;
	this.type				= type;
	this.default_value		= default_value;
	this.allow_null			= allow_null;
	this.object_validations	= object_validations;
	this.constraints		= constraints;


	this.Validate_Number	= Validator_Validate_Number;
	this.Validate_String	= Validator_Validate_String;
	this.Validate_Boolean	= Validator_Validate_Boolean;
	this.Validate_Folder	= Validator_Validate_Folder;
	this.Validate_Object	= Validator_Validate_Object;
	this.Validate			= Validator_Validate;
}


function Validator_Validate_Number(
	data )
{
	// null check
	if( ( this.allow_null	==	true	) &&
		( data				===	null	) )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}


	// type check
	if( ( typeof data	!==	"number"	) ||
		( data			==	NaN			) ||
		( data			==	Infinity	) )
	{
		return {
			errors:		[ this.name + " is not a number" ],
			corrected:	this.default_value,
		};
	}


	// constraints check
	if( typeof this.constraints == "undefined" )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}
	else
	{
		if( ( typeof this.constraints.minimum	!=	"undefined"					) &&
			( data								<	this.constraints.minimum	) )
		{
			return {
				errors:		[ this.name + " exceeds the minimum (" + this.constraints.minimum.toString() + ")" ],
				corrected:	this.constraints.minimum,
			};
		}
		else if(	( typeof this.constraints.maximum	!=	"undefined"					) &&
					( data								>	this.constraints.maximum	) )
		{
			return {
				errors:		[ this.name + " exceeds the maximum (" + this.constraints.maximum.toString() + ")" ],
				corrected:	this.constraints.maximum,
			};
		}
		else
		{
			return {
				errors:		[],
				corrected:	data,
			};
		}
	}
}


function Validator_Validate_String(
	data )
{
	// null check
	if( ( this.allow_null	==	true	) &&
		( data				===	null	) )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}


	// type check
	if( typeof data !== "string" )
	{
		return {
			errors:		[ this.name + " is not a string" ],
			corrected:	this.default_value,
		};
	}


	// constraints check
	if( typeof this.constraints == "undefined" )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}
	else
	{
		if( ( typeof this.constraints.allow_empty	!=	"undefined"	) &&
			( this.allow_empty						==	false		) &&
			( data									===	""			) )
		{
			return {
				errors:		[ this.name + " is empty" ],
				corrected:	this.default_value,
			};
		}
		else if( ( typeof this.constraints.length != "undefined" ) && ( data.length > this.constraints.length ) )
		{
			return {
				errors:		[ this.name + " exceeds the maximum length (" + this.constraints.length.toString() + ")" ],
				corrected:	this.default_value,
			};
		}
		else if(	( typeof this.constraints.regex	!=	"undefined"	) &&
					( this.constraints.regex		!=	true		) )
		{
			return {
				errors:		[ this.name + " does not match the pattern (" + this.constraints.regex.toString() + ")" ],
				corrected:	this.default_value,
			};
		}
		else
		{
			return {
				errors:		[],
				corrected:	data,
			};			
		}
	}
}


function Validator_Validate_Boolean(
	data )
{
	// null check
	if( ( this.allow_null	==	true	) &&
		( data				===	null	) )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}


	// type check
	if( typeof data !== "boolean" )
	{
		return {
			errors:		[ this.name + " is not a boolean" ],
			corrected:	this.default_value,
		};
	}
	else
	{
		return {
			errors:		[],
			corrected:	data,
		};			
	}
}


function Validator_Validate_Folder(
	data )
{
	// null check
	if( ( this.allow_null	==	true	) &&
		( data				===	null	) )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}


	// type check
	if( typeof data !== "string" )
	{
		return {
			errors:		[ this.name + " is not a string" ],
			corrected:	Path.normalize( this.default_value + "//" ),
		};
	}
	else
	{
		return {
			errors:		[],
			corrected:	Path.normalize( data + "//" ),
		};
	}
}


function Validator_Validate_Object(
	data )
{
	// null check
	if( ( this.allow_null	==	true	) &&
		( data				===	null	) )
	{
		return {
			errors:		[],
			corrected:	data,
		};
	}


	// type check
	var errors		= [];
	var corrected	= {};

	if( ( typeof data			===	"object"	) &&
		( data					!==	null		) &&
		( Array.isArray( data )	==	false		) )
	{
		corrected = JSON.parse( JSON.stringify( data ) );
	}
	else
	{
		errors.push( "defaults" );
	}


	// validate the object's keys
	this.object_validations.forEach( function( value, key )
	{
		var retval = value.Validate( corrected[ key ] );
		errors = errors.concat( retval.errors );
		corrected[ key ] = retval.corrected;
	});


	// only 1 error if we're using the default object
	if( ( errors.length	>	0			) &&
		( errors[ 0 ]	==	"defaults"	) )
	{
		errors = [ this.name + " is not an object" ];
	}


	return {
		errors:		errors,
		corrected:	corrected,
	};
}


function Validator_Validate(
	data )
{
	switch( this.type )
	{
		case "number":
			return this.Validate_Number( data );

		case "string":
			return this.Validate_String( data );

		case "boolean":
			return this.Validate_Boolean( data );

		case "folder":
			return this.Validate_Folder( data );

		case "object":
			return this.Validate_Object( data );

		default:
			return { errors: [], corrected: data };
	}
}


// /**
//  * returns a list of error strings if the data does not meet the validation requirements
//  */
// /**
//  * validates the given data
//  *
//  * @param {*}        data     the data to validate
//  * @param {Function} callback a callback function({errors, data}) that returns a list of errors and the corrected data
//  */
// function Validator_Validate(
// 	data,
// 	callback )
// {
// 	// setup
// 	var errors		= [];
// 	var corrected	= JSON.parse( JSON.stringify( this.default_value ) );


// 	// existence check
// 	if( ( data == null ) || ( data_type === "undefined" ) || ( data == NaN ) || ( data == Infinity ) )
// 	{
// 		errors.push( new Error( this.name + " not found" ) );
// 		callback( errors, corrected );
// 		return;
// 	}


// 	// type check
// 	switch( this.type )
// 	{
// 		default:
// 			break;
// 	}



// 	// constraints check
// 	switch( this.type )
// 	{
// 		default:
// 			break;
// 	}



// 	callback( errors, corrected );
// }


// /**
//  * returns the default value if the data does not meet the validation requirements
//  */
// function Validator_Lazy_Validate(
// 	data )
// {
// 	var data_type = typeof data;


// 	// existence check
// 	if( ( data == null ) || ( data_type === "undefined" ) || ( data == NaN ) || ( data == Infinity ) )
// 		return this.default_value;


// 	// type check
// 	switch( this.type )
// 	{
// 		case "object":
// 			if( ( data_type == "object" ) && ( Array.isArray( data ) == false ) )
// 				return data;
// 			else
// 				return this.default_value;
// 			break;


// 		case "array":
// 			if( Array.isArray( data ) == true )
// 				return data;
// 			else
// 				return this.default_value;
// 			break;


// 		case "folder":
// 			if( data_type == "string" )
// 				return Path.normalize( data + "//" );
// 			else
// 				return Path.normalize( this.default_value + "//" );
// 			break;


// 		default:
// 			if( data_type !== this.type )
// 				return this.default_value;
// 			break;
// 	}
	


// 	// constraints check
// 	if( !this.constraints )
// 		return data;

// 	switch( this.type )
// 	{
// 		case "number":
// 			return ( ( typeof this.Validate_Number( data ) != "undefined" ) ? this.default_value : data );
// 	}


// 	return data;
// }


// /**
//  * validates numerical data
//  */
// function Validator_Validate_Number(
// 	data )
// {
// 	if( ( typeof this.constraints.minimum == "number" ) && ( data < this.constraints.minimum ) )
// 		return "minimum";
// 	else if( ( typeof this.constraints.maximum == "number" ) && ( data > this.constraints.maximum ) )
// 		return "maximum";
// 	else
// 		return;
// }


// event functions


// functions


// exports
module.exports = Validator;


// executable code


/** eof */