/**
 * @file stds.js
 *
 * @description a set of standard values and functions
 */


// node requires


// 3rd party requires


// project requires


// type declarations


// constants


// global variables


// variables


// type functions


// event functions


// functions
function Clear_Empty_Files(
	directory,
	recursive,
	callback )
{
	require( "child_process" ).exec( "find " + directory + " -type f -empty -print -delete" + ( ( recursive == true ) ? "" : "maxdepth 1" ), ( error, stdout, stderr ) => {
		debug( "hi" );
		callback( error, stdout, stderr );
	});
}


function Clear_Empty_Folders(
	directory,
	recursive,
	callback )
{
	require( "child_process" ).exec( "find " + directory + " -type d -empty -print -delete" + ( ( recursive == true ) ? "" : "maxdepth 1" ), callback );
}


// exports
module.exports =
{
	Clear_Empty_Files:		Clear_Empty_Files,
	Clear_Empty_Folders:	Clear_Empty_Folders,
};


// executable code


/** eof */