/**
 * @file app.js
 *
 * @description the main file for the RFDCS server applications
 */


// node requires


// 3rd party requires


// project requires
const App_Data	= require( "./app-data.js"	);
const Cmd_Line	= require( "./cmd-line.js"	);


// type declarations


// constants


// global variables


// variables
var shutdown = false; /**< true if the app received a shutdown signal */


// type functions


// event functions
/**
 * application shutdown handler
 */
function Shutdown()
{
	// no double shutdowns
	if( shutdown == true )
		return;
	else
		shutdown = true;


	debug( "app shutting down" );
	App_Data.Module_Shutdown( Module_Shutdown_Complete );
}


/**
 * module initialization completion handler
 */
function Module_Initialize_Complete()
{
	debug( "app initialized" );
	debug( "app starting" );
	App_Data.Module_Start( Module_Start_Complete );
}


/**
 * module startup completion handler
 */
function Module_Start_Complete()
{
	debug( "app started" );
}


/**
 * module shutdown completion handler
 */
function Module_Shutdown_Complete()
{
	debug( "app shutdown" );
}


// functions
/**
 * application startup procedure
 */
function Start()
{
	debug( "app starting" );


	// setup shutdown
	process.on( "exit",		Shutdown );
	process.on( "SIGINT",	Shutdown );
	process.on( "SIGTERM",	Shutdown );


	// process command line args
	Cmd_Line.Parse();


	// initialize
	debug( "app initializing" );
	App_Data.Module_Initialize( Module_Initialize_Complete );
}


// exports
module.exports =
{
		
};


// executable code
Start();


/** eof */