/**
 * @file 
 *
 * @description
 */


// node requires


// 3rd party requires
const Debug	= require( "debug"	);


// project requires
const Cmd_Line_Option	= require( "./cmd-line-option.js"	);
const Validator			= require( "./validator.js"			);
const Configuration		= require( "./configuration.js"		);
const Logging			= require( "./logging.js"			);


// type declarations


// constants
const name			= "";							/**< the module's name			*/
const debug_module	= Debug( debug_prefix + name );	/**< a debugger for the module	*/


// global variables


// variables
var debug_options =
[
	{ name: debug_module.namespace, summary: "the " + name + " module debug option" },
]; /**< the module's debug options */

var cmd_line_options =
[
	// new Cmd_Line_Option(
	// 	"name",
	// 	"alias",
	// 	type,
	// 	multiple,
	// 	defaultValue,
	// 	"description",
	// 	typeLabel,
	// 	execution_function,
	// 	exit_after ),
]; /**< the module's cmd line options */

var config	= {};	/**< the module's configuration	*/


// type functions


// event functions


// functions
/**
 * the module's initialization function
 *
 * @param {Function} callback a callback function() to notify that the module was initialized
 */
function Initialize(
	callback )
{
	Load_Configuration();
	callback();
}


/**
 * loads the module's configuration
 */
function Load_Configuration()
{
	// get the module's configuration
	config = Configuration.Get( name );


	// validate the configuration
	var map_module = new Map();

	// map_module.set( , new Validator(
	// ));


	var validator = new Validator(
		name,
		"object",
		null,
		false,
		map_module );

	var retval = validator.Validate( config );

	config = retval.corrected;

	if( retval.errors.length > 0 )
		debug_module( "configuration errors\n%O", retval.errors );


	// check if we should save anything
	Configuration.Update( name, config );
}


/**
 * the module's start function
 *
 * @param {Function} callback a callback function() to notify that the module was started
 */
function Start(
	callback )
{
	callback();
}


/**
 * the module's shutdown function
 *
 * @param {Function} callback a callback function() to notify that the module was shutdown
 */
function Shutdown(
	callback )
{
	callback();
}


// exports
module.exports =
{
	// App_Module
	name:				name,
	debug_options:		debug_options,
	cmd_line_options:	cmd_line_options,

	Initialize:			Initialize,
	Start:				Start,
	Shutdown:			Shutdown,


	// custom
};


// executable code


/** eof */