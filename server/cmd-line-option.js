/**
 * @file cmd-line-option.js
 *
 * @description class definition for cmd line options
 */


// node requires


// 3rd party requires


// project requires


// type declarations


// constants


// global variables


// variables


// type functions
/**
 * class to handle command line parameters
 *
 * @param {String}   name          the option's name
 * @param {String}   alias         the option's alias
 * @param {Function} type          the option's type
 * @param {Boolean}  multiple      true if the option takes in a list of values
 * @param {*}        defaultValue  the option's default value
 * @param {String}   description   the option's description
 * @param {String}   typeLabel     defines how the type should be printed in the help guide
 * @param {Function} execute       the option's execution function
 * @param {Boolean}  exit_after    true to exit the process after execution
 */
function Cmd_Line_Option(
    name,
    alias,
    type,
    multiple,
    defaultValue,
    description,
    typeLabel,
    execute,
    exit_after )
{
    // variables
    this.name           = name;
    this.alias          = alias;
    this.type           = type;
    this.multiple       = multiple;
    this.description    = description;
    this.typeLabel      = typeLabel;
    this.exit_after     = exit_after;

    if( defaultValue != null )
        this.defaultValue = defaultValue;


    // functions
    this.Custom_Execute = execute;
    this.Execute        = Cmd_Line_Option_Execute;
}


/**
 * the abstracted execution function for Cmd_Line_Option
 */
function Cmd_Line_Option_Execute(
    args )
{
    debug( "processing command line option : " + this.name );
    this.Custom_Execute( args );

    if( this.exit_after == true )
        process.exit();
}


// event functions


// functions


// exports
module.exports = Cmd_Line_Option;


// executable code


/** eof */