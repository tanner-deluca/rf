/**
 * @file configuration.js
 *
 * @description file for handling app configuration
 */


// node requires
const FS	= require( "fs"	);


// 3rd party requires
const Debug	= require( "debug"	);


// project requires
const Cmd_Line_Option	= require( "./cmd-line-option.js"	);
const Validator			= require( "./validator.js"			);


// type declarations


// constants
const name							= "configuration";						/**< the module's name								*/
const debug_module					= Debug( debug_prefix + name );			/**< a debugger for the configuration				*/
const configuration_folder			= "configuration/";						/**< the default folder for the configuration file	*/
const default_configuration_file	= configuration_folder + "std.json";	/**< the default configuration file path			*/

const config_save_delay	= "save_delay";	/**< configuration variable for the time (ms) between an update and saving	*/


// global variables


// variables
var debug_options =
[
	{ name: debug_module.namespace, summary: "the " + name + " module debug option" },
]; /**< the module's debug options */

var cmd_line_options =
[
	new Cmd_Line_Option(
		"config",
		"c",
		String,
		false,
		null,
		"sets the configuration file",
		null,
		Cmd_Line_Config,
		false ),
]; /**< the module's cmd line options */

var config			= {};		/**< the module's configuration							*/
var file			= "";		/**< the configuration file to use						*/
var backup			= "";		/**< the backup configuration file to use				*/
var configuration	= {};		/**< the loaded configuration							*/
var saving			= false;	/**< true if the configuration is being saved			*/
var save_timer		= null;		/**< timer to save the configuration					*/
var dead			= false;	/**< true if the configuration module died				*/


// type functions


// event functions
/**
 * handler for the --config command line argument
 */
function Cmd_Line_Config(
	args )
{
	debug( args );
	file	= args;
	backup	= args + ".bak";
	debug_module( "using file " + file );
}


/**
 * handler for the delay to save an update
 */
function Save_Update()
{
	if( Save_Configuration() == true )
		save_timer = setTimeout( Save_Update, config[ config_save_delay ] );
	else
		save_timer = null;
}


// functions
/**
 * the module's initialization function
 *
 * @param {Function} callback a callback function() to notify that the module was initialized
 */
function Initialize(
	callback )
{
	// set the default configuration file
	if( file == "" )
	{
		// set the default file
		file	= default_configuration_file;
		backup	= default_configuration_file + ".bak";


		// create the default file if it doesn't exist
		if( FS.existsSync( file ) == false )
		{
			debug_module( "default file (%s) doesn't exist\nchecking for a backup (%s)", file, backup );


			// check for a backup
			if( FS.existsSync( backup ) == false )
			{
				debug_module( "backup doesn't exist\ncreating a new default file" );

				try
				{
					FS.mkdirSync( configuration_folder, { recursive: true } );
					FS.writeFileSync( file, "{}" );
				}
				catch( error )
				{
					dead = true;
					require( "./logging.js" ).Fatal( `failed to create the default configuration file (${file})\n${error}` );
				}
			}
			else
			{
				Restore_Configuration_File();
			}
		}
	}


	Load_Configuration_File();
	Load_Configuration();
	callback();
}


/**
 * loads the module's configuration
 */
function Load_Configuration()
{
	// get the module's configuration
	config = Get( name );


	// validate the configuration
	var map_module = new Map();

	map_module.set( config_save_delay, new Validator(
		config_save_delay,
		"number",
		300000,
		null,
		false,
		{ minimum: 0, maximum: 600000 }
	));


	var validator = new Validator(
		name,
		"object",
		null,
		false,
		map_module );

	var retval = validator.Validate( config );

	config = retval.corrected;

	if( retval.errors.length > 0 )
		debug_module( "configuration errors\n%O", retval.errors );


	// check if we should save anything
	Update( name, config );
}


/**
 * the module's shutdown function
 *
 * @param {Function} callback a callback function() to notify that the module was shutdown
 */
function Shutdown(
	callback )
{
	// make sure we didn't die while loading the config
	if( dead == true )
	{
		callback();
		return;
	}


	// check if a final save is required
	var save_required = false;

	if( save_timer != null )
	{
		clearTimeout( save_timer );
		save_required = true;
		debug_module( "final save required" );
	}


	// wait for a current save to finish
	Wait_For_Save( function()
	{
		if( save_required == true )
		{
			try
			{
				FS.writeFileSync( file, JSON.stringify( configuration, null, 4 ) );
				FS.copyFileSync( file, backup );
				debug_module( "final save success" );
			}
			catch( error )
			{
				debug_module( "final save failed\n%s", error.toString() );
			}
		}
		else
		{
			// make a backup
			try
			{
				FS.copyFileSync( file, backup );
				debug_module( "backup (%s) created", backup );
			}
			catch( error )
			{
				debug_module( "failed to create backup(%s)\n%s", backup, error.toString() );
			}
		}


		callback();
	});
}


/**
 * loads the configuration file
 */
function Load_Configuration_File()
{
	try
	{
		configuration = require( "./" + file );
		debug_module( "configuration loaded" );
	}
	catch( error )
	{
		debug_module( "configuration file (%s) failed to load\n%s", file, error.toString() );
		Restore_Configuration_File();
	}
}


/**
 * restores the configuration file from a backup file
 */
function Restore_Configuration_File()
{
	debug_module( "attempting to restore configuration (%s) from backup (%s)", file, backup );

	try
	{
		debug_module( "loading backup" );
		configuration = JSON.parse( FS.readFileSync( backup ) );
		debug_module( "backup loaded, attempting to restore configuration" );


		try
		{
			FS.copyFileSync( backup, file );
			debug_module( "configuration restored" );
		}
		catch( error )
		{
			dead = true;
			require( "./logging.js" ).Fatal( `unable to restore configuration (${file}) from backup (${backup})\n${error}` );
		}
	}
	catch( error )
	{
		dead = true;
		require( "./logging.js" ).Fatal( `unable to load backup configuration (${backup})\n${error}` );
	}
}


/**
 * saves the current configuration to file
 */
function Save_Configuration()
{
	// check if we're currently saving
	if( saving == true )
		return true;
	

	// start saving
	saving = true;


	// write the backup
	debug_module( "creating the backup" );

	FS.copyFile( file, backup, function( error1 ) {
		if( error1 )
		{
			require( "./logging.js" ).Error( "configuration file (" + file + ") backup (" + backup + ") failure\n" + error1.toString() );
			saving = false;
		}
		else
		{
			debug_module( "backup created\nsaving configuration" );
			FS.writeFile( file, JSON.stringify( configuration, null, 4 ), function( error2 ) {
				if( error2 )
					require( "./logging.js" ).Error( "configuration file (" + file + ") save failure\n" + error2.toString() );

				debug_module( "configuration saved" );
				saving = false;
			});
		}
	});


	return false;
}


/**
 * notification for when a save in progress is finished
 */
function Wait_For_Save(
	callback )
{
	if( saving == true )
		setTimeout( Wait_For_Save.bind( null, callback ), 1000 );
	else
		callback();
}


/**
 * returns a module's configurations
 */
function Get(
	module_name )
{
	if( !configuration[ module_name ] )
		configuration[ module_name ] = {};

	return JSON.parse( JSON.stringify( configuration[ module_name ] ) );
}


/**
 * updates a module's configuration
 *
 * @param {String} module_name   the updated module's name
 * @param {Object} module_config the updated configuration
 */
function Update(
	module_name,
	module_config )
{
	// check if an update occurred
	if( JSON.stringify( configuration[ module_name ] ) === JSON.stringify( module_config ) )
		return;
	

	// update
	configuration[ module_name ] = JSON.parse( JSON.stringify( module_config ) );


	// flag for saving
	clearTimeout( save_timer );
	save_timer = setTimeout( Save_Update, config[ config_save_delay ] );
}


// exports
module.exports =
{
	// App_Module
	name:				name,
	debug_options:		debug_options,
	cmd_line_options:	cmd_line_options,

	Initialize:			Initialize,
	Shutdown:			Shutdown,


	// custom
	Get:				Get,
	Update:				Update,
};


// executable code


/** eof */