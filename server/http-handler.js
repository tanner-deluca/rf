/**
 * @file 
 *
 * @description
 */


// node requires
const HTTP	= require( "http"	);
const HTTPS	= require( "https"	);


// 3rd party requires
const Debug			= require( "debug"					);
const Winston		= require( "winston"				);
const Express		= require( "express"				);


// project requires
const Cmd_Line_Option	= require( "./cmd-line-option.js"	);
const Validator			= require( "./validator.js"			);
const Configuration		= require( "./configuration.js"		);
const Logging			= require( "./logging.js"			);


// type declarations


// constants
const name			= "http";						/**< the module's name			*/
const debug_module	= Debug( debug_prefix + name );	/**< a debugger for the module	*/

const config_http_port		= "http_port";		/**< configuration variable for the HTTP port	*/
const config_https_port		= "https_port";		/**< configuration variable for the HTTPS port	*/
const config_http_logger	= "http_logger";	/**< configuration variable for the http logger	*/


// global variables


// variables
var debug_options =
[
	{ name: debug_module.namespace, summary: "the " + name + " module debug option" },
]; /**< the module's debug options */

var cmd_line_options =
[
	// new Cmd_Line_Option(
	// 	"name",
	// 	"alias",
	// 	type,
	// 	multiple,
	// 	defaultValue,
	// 	"description",
	// 	typeLabel,
	// 	execution_function,
	// 	exit_after ),
]; /**< the module's cmd line options */

var config			= {};			/**< the module's configuration	*/
var app				= Express();	/**< the express http handler	*/
var http_server		= null;			/**< the http server			*/
var https_server	= null;			/**< the https server			*/
var get_routes		= new Map();
var post_routes		= new Map();
var logger_http		= null;


// type functions


// event functions
function Debug_Log(
	request,
	response,
	next )
{
	// setup
	var message		= `received request (${request.path})`;
	var log_debug	= true;
	var log_file	= true;
	var error		= false;
	var meta		= {
		protocol:	request.protocol,
		method:		request.method,
		url:		request.originalUrl,
		// TODO: user
		body:		"",
		error:		"",
	};


	// handle route logging
	switch( request.method )
	{
		case "GET":
			if( get_routes.has( request.path ) == true )
			{
				var route = get_routes.get( request.path );
			}
			else
			{
				error		== true;
				meta.error	== "unsupported route";
			}
			break;


		case "POST":
			if( post_routes.has( request.path ) == true )
			{
				var route = post_routes.get( request.path );

				if( route.log_body == true )
				{
					// TODO: log body
				}
			}
			else
			{
				error		== true;
				meta.error	== "unsupported route";
			}
			break;


		default:
			error		== true;
			meta.error	== "unsupported method";
			break;
	}


	// log
	if( log_debug == true )
	{
		if( error == true )
			debug_module( "ERROR::%s\n%O", message, meta );
		else
			debug_module( "%s\n%O", message, meta );
	}

	if( log_file == true )
	{
		if( error == true )
			logger_http.error( message, meta );
		else
			logger_http.info( message, meta );
	}


	next();
}


/**
 * ping route handler
 *
 * @param {Object} request  express request object
 * @param {Object} response express response object
 */
function Ping(
	request,
	response )
{
	response.send( "" );
}


// functions
/**
 * the module's initialization function
 *
 * @param {Function} callback a callback function() to notify that the module was initialized
 */
function Initialize(
	callback )
{
	Load_Configuration();


	// setup debugging
	logger_http = Logging.Add( config[ config_http_logger ], Winston.format.combine( Winston.format.timestamp({ format: Logging.Timestamp_Format }), Winston.format.json() ) );
	app.use( Debug_Log );


	// setup GET routes
	get_routes.set( "/", { handler: Ping,	log_debug:	true,	log_file:	true	} );

	get_routes.forEach( function( value, key )
	{
		app.get( key, value.handler );
	});


	// setup POST routes
	post_routes.set( "/", { handler: Ping,	log_debug:	true,	log_file:	true,	log_body: false } );

	post_routes.forEach( function( value, key )
	{
		app.post( key, value.handler );
	});


	callback();
}


/**
 * loads the module's configuration
 */
function Load_Configuration()
{
	// get the module's configuration
	config = Configuration.Get( name );


	// validate the configuration
	var map_module		= new Map();
	var map_http_logger	= Logging.Validation_Map(
		"http",
		false,
		true,
		false,
		true,
		null,
		"YYYY-MM-DD",
		true,
		"10m",
		"5d"
	);

	map_module.set( config_http_port, new Validator(
		config_http_port,
		"number",
		8080,
		false,
		null,
		{ minimum: 0 }
	));

	map_module.set( config_https_port, new Validator(
		config_https_port,
		"number",
		null,
		true,
		null,
		{ minimum: 0 }
	));

	map_module.set( config_http_logger, new Validator(
		config_http_logger,
		"object",
		null,
		false,
		map_http_logger,
	));


	var validator = new Validator(
		name,
		"object",
		null,
		false,
		map_module );

	var retval = validator.Validate( config );

	config = retval.corrected;

	if( retval.errors.length > 0 )
		debug_module( "configuration errors\n%O", retval.errors );


	// check if we should save anything
	Configuration.Update( name, config );
}


/**
 * the module's start function
 *
 * @param {Function} callback a callback function() to notify that the module was started
 */
function Start(
	callback )
{
	var http_port	= config[ config_http_port ];
	var https_port	= config[ config_https_port ];


	try
	{
		debug_module( "listening on port " + http_port.toString() );
		http_server = HTTP.createServer( app ).listen( http_port );
		debug_module( "success" );
	}
	catch( error )
	{
		Logging.Fatal( "failed to open HTTP port (" + http_port.toString() + ")\n" + error.toString() );
	}



	if( https_port != null )
	{
		try
		{
			debug_module( "listening on port " + https_port.toString() );
			https_server = HTTPS.createServer( app ).listen( https_port );
			debug_module( "success" );
		}
		catch( error )
		{
			Logging.Fatal( "failed to open HTTPS port (" + https_port.toString() + ")\n" + error.toString() );
		}
	}


	callback();
}


/**
 * the module's shutdown function
 *
 * @param {Function} callback a callback function() to notify that the module was shutdown
 */
function Shutdown(
	callback )
{
	if( http_server == null )
	{
		if( https_server == null )
		{
			callback();
		}
		else
		{
			https_server.close( function()
			{
				callback();
			});
		}
	}
	else
	{
		http_server.close( function()
		{
			if( https_server == null )
			{
				callback();
			}
			else
			{
				https_server.close( function()
				{
					callback();
				});
			}
		});
	}
}


// exports
module.exports =
{
	// App_Module
	name:				name,
	debug_options:		debug_options,
	cmd_line_options:	cmd_line_options,

	Initialize:			Initialize,
	Start:				Start,
	Shutdown:			Shutdown,


	// custom
};


// executable code


/** eof */