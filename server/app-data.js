/**
 * @file app-data.js
 *
 * @description contains information about the application
 */


// node requires


// 3rd party requires
const Debug	= require( "debug"	);


// project requires
const App_Module		= require( "./app-module.js"		);
const Cmd_Line_Option	= require( "./cmd-line-option.js"	);


// type declarations


// constants
const app_name			= require( "./package.json" ).name;			/**< the application's name			*/
const app_version		= require( "./package.json" ).version;		/**< the application's version		*/
const app_description	= require( "./package.json" ).description;	/**< the application's description	*/
const name				= "app-data";								/**< the module's name				*/


// global variables
debug_prefix	= app_name + ":";		/**< a prefix for additional debuggers	*/
debug			= Debug( app_name );	/**< a debugger for general info		*/


// variables
var debug_options =
[
	{ name: debug.namespace, summary: "the default debug option"	},
]; /**< the module's debug options */

var cmd_line_options =
[
	new Cmd_Line_Option(
		"version",
		"v",
		Boolean,
		false,
		null,
		"prints the application version",
		null,
		Cmd_Line_Version,
		true ),
]; /**< the module's cmd line options */

var app_modules = []; /**< a list of modules the application uses */


// type functions


// event functions
/**
 * handler for the --version command line argument
 */
function Cmd_Line_Version(
	args )
{
	console.log( app_version );
}


/**
 * module initialization completion handler
 */
function Module_Initialize_Complete(
	modules,
	index,
	callback )
{
	index++;

	if( index >= modules.length )
	{
		debug( "module initialization complete" );
		callback();
	}
	else
	{
		modules[ index ].module.Initialize( Module_Initialize_Complete.bind( null, modules, index, callback ) );
	}
}


/**
 * module startup completion handler
 */
function Module_Start_Complete(
	modules,
	index,
	callback )
{
	index++;

	if( index >= modules.length )
	{
		debug( "module startup complete" );
		callback();
	}
	else
	{
		modules[ index ].module.Start( Module_Start_Complete.bind( null, modules, index, callback ) );
	}
}


/**
 * module shutdown completion handler
 */
function Module_Shutdown_Complete(
	modules,
	index,
	callback )
{
	index++;

	if( index >= modules.length )
	{
		debug( "module shutdown complete" );
		callback();
	}
	else
	{
		modules[ index ].module.Shutdown( Module_Shutdown_Complete.bind( null, modules, index, callback ) );
	}
}


// functions
/**
 * initializes all modules
 *
 * @param {Function} callback function() to call when initialization is completed
 */
function Module_Initialize(
	callback )
{
	debug( "module initialization started" );

	var sorted = Sort_Modules( "initialize" );
	sorted[ 0 ].module.Initialize( Module_Initialize_Complete.bind( null, sorted, 0, callback ) );
}


/**
 * starts all modules
 *
 * @param {Function} callback function() to call when startup is completed
 */
function Module_Start(
	callback )
{
	debug( "module startup started" );

	var sorted = Sort_Modules( "start" );
	sorted[ 0 ].module.Start( Module_Start_Complete.bind( null, sorted, 0, callback ) );
}


/**
 * shuts down all modules
 *
 * @param {Function} callback function() to call when shutdown is completed
 */
function Module_Shutdown(
	callback )
{
	debug( "module shutdown started" );

	var sorted = Sort_Modules( "shutdown" ).reverse();
	sorted[ 0 ].module.Shutdown( Module_Shutdown_Complete.bind( null, sorted, 0, callback ) );
}


function Sort_Modules(
	order )
{
	var sorted = app_modules.slice();
	var i;
	var j;
	var j1;

	for( i = 0; i < sorted.length; i++ )
	{
		for( j = 0, j1 = 1; j < sorted.length - i - 1; j++, j1++ )
		{
			if( sorted[ j ][ order ] > sorted[ j1 ][ order ] )
			{
				var tmp = sorted[ j ];
				sorted[ j ]		= sorted[ j1 ];
				sorted[ j1 ]	= tmp;
			}
		}
	}


	return sorted;
}


// exports
module.exports =
{
	// App_Module
	name:				name,
	debug_options:		debug_options,
	cmd_line_options:	cmd_line_options,


	// custom
	app_name:			app_name,
	app_version:		app_version,
	app_description:	app_description,
	app_modules:		app_modules,

	Module_Initialize:	Module_Initialize,
	Module_Start:		Module_Start,
	Module_Shutdown:	Module_Shutdown,
};


// executable code
// app-data is itself a module
module.exports.app_modules = app_modules =
[
	{ module: new App_Module( module.exports					),	initialize:	0,	start: 0,	shutdown: 0	},
	{ module: new App_Module( require( "./configuration.js"	)	),	initialize:	1,	start: 1,	shutdown: 2	},
	{ module: new App_Module( require( "./logging.js"		)	),	initialize:	2,	start: 2,	shutdown: 1	},
	{ module: new App_Module( require( "./http-handler.js"	)	),	initialize:	3,	start: 3,	shutdown: 3	},
];

/** eof */